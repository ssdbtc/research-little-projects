<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
ini_set('max_execution_time', '-1');
ini_set('memory_limit', '2048M');
date_default_timezone_set('Europe/London');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/../PHPExcel/Classes/PHPExcel.php';


class Lekeres {

   public $hiba = "";
   private $timestart;
   private $numElement;
   private $objPHPExcel;
   private $objPHPExcelMerged;
   private $txtExperimentName;
   private $itrLogFile = false;

   public function __construct($exp) {

      $this->timestart = microtime(true);
      
      $txtFiletype = ".csv";
      $numExperiment = $exp;

      $txtFolderName = "histogram" . $numExperiment;
      $this->txtExperimentName = $txtFolderName;

      self::LogMe(date('H:i:s') . " START " . $this->timestart);

      $arrExperimentInfo = [];

      $arrExperimentInfo["01"] = array();
      $arrExperimentInfo["01"]["members"] = ["CTRL", "TPT1"];
      $arrExperimentInfo["01"]["hours"] = ["72"];
      $arrExperimentInfo["01"]["phases"] = [0 => "", 1 => "_G0", 2 => "_G1", 3 => "_G0-G1", 4 => "_G2-M", 5 => "_S"];

      $arrExperimentInfo["02"] = array();
      $arrExperimentInfo["02"]["members"] = ["CTRL", "TPT1", "eIF6", "VBP1"];
      $arrExperimentInfo["02"]["hours"] = ["72", "120"];
      $arrExperimentInfo["02"]["phases"] = [0 => "", 1 => "_G0", 2 => "_G1", 3 => "_G0-G1", 4 => "_G2-M", 5 => "_S"];

      $arrExperimentInfo["03"] = array();
      $arrExperimentInfo["03"]["members"] = ["CTRL", "TPT1", "eIF6", "MCM7", "VBP1"];
      $arrExperimentInfo["03"]["hours"] = ["40", "48", "64", "72", "120"];
      $arrExperimentInfo["03"]["phases"] = [0 => "", 1 => "_G0", 2 => "_G1", 3 => "_G0-G1", 4 => "_G2-M", 5 => "_S"];

      $arrExperimentInfo["04"] = array();
      $arrExperimentInfo["04"]["members"] = ["CTRL", "TPT1", "eIF6", "MCM7", "VBP1"];
      $arrExperimentInfo["04"]["hours"] = ["24", "40", "48", "72"];
      $arrExperimentInfo["04"]["phases"] = [0 => "", 1 => "_G0", 2 => "_G1", 3 => "_G0-G1", 4 => "_G2-M", 5 => "_S"];

      $arrExperimentInfo["05"] = array();
      $arrExperimentInfo["05"]["members"] = ["CTRL1x", "eIF6", "MCM7", "VBP11x"];
      $arrExperimentInfo["05"]["hours"] = ["24", "40", "48", "62", "72"];
      $arrExperimentInfo["05"]["phases"] = [0 => "", 1 => "_G0", 2 => "_G1", 3 => "_G0-G1", 4 => "_G2-M", 5 => "_S"];

      $arrExperimentInfo["06"] = array();
      $arrExperimentInfo["06"]["members"] = ["CTRL1", "TPT1-01", "TPT1-05", "TPT1-06", "TPT1-16"];
      $arrExperimentInfo["06"]["hours"] = ["72"];
      $arrExperimentInfo["06"]["phases"] = [0 => "", 3 => "_G0-G1", 4 => "_G2-M", 5 => "_S"];

      $arrExperimentInfo["07"] = array();
      $arrExperimentInfo["07"]["members"] = ["CTRL1", "TPT1-01", "TPT1-05", "TPT1-06", "TPT1-16"];
      $arrExperimentInfo["07"]["hours"] = ["72"];
      $arrExperimentInfo["07"]["phases"] = [0 => "", 3 => "_G0-G1", 4 => "_G2-M", 5 => "_S"];

      $arrExperimentInfo["14"] = array();
      $arrExperimentInfo["14"]["members"] = ["CTRL1", "TPT1", "MCM71", "MCM72" ];
      $arrExperimentInfo["14"]["hours"] = ["72"];
      $arrExperimentInfo["14"]["phases"] = [0 => "", 1 => "_G0", 2 => "_G1", 3 => "_G0-G1", 4 => "_G2-M", 5 => "_S"];

      $arrExperimentInfo["15"] = array();
      $arrExperimentInfo["15"]["members"] = ["CTRL", "TPT1", "VBP1" ];
      $arrExperimentInfo["15"]["hours"] = ["72"];
      $arrExperimentInfo["15"]["phases"] = [0 => "", 3 => "_G0-G1", 4 => "_G2-M", 5 => "_S"];

      $arrExperimentInfo["10-1"] = array();
      $arrExperimentInfo["10-1"]["members"] = ["CTRL1", "CTRL2", "MCM71", "MCM72", "MCM4"];
      $arrExperimentInfo["10-1"]["hours"] = ["72"];
      $arrExperimentInfo["10-1"]["phases"] = [0 => "", 3 => "_G0-G1", 4 => "_G2-M", 5 => "_S"];

      $arrExperimentInfo["10-2"] = array();
      $arrExperimentInfo["10-2"]["members"] = ["CTRL2", "TPT1-01", "TPT1-05", "TPT1-06", "TPT1-16"];
      $arrExperimentInfo["10-2"]["hours"] = ["72"];
      $arrExperimentInfo["10-2"]["phases"] = [0 => "", 3 => "_G0-G1", 4 => "_G2-M", 5 => "_S"];

      $arrExperimentInfo["10-3"] = array();
      $arrExperimentInfo["10-3"]["members"] = ["CTRL1", "PFD2", "PFD5", "VBP1"];
      $arrExperimentInfo["10-3"]["hours"] = ["72"];
      $arrExperimentInfo["10-3"]["phases"] = [0 => "", 3 => "_G0-G1", 4 => "_G2-M", 5 => "_S"];

      $arrMCM7experiments = array("131212", "140128", "140206", "140416", "140627", "140801", "160420", "1502051", "1502052", "1603291", "1603292");

      $arrExperimentInfo["mcm7"] = array();
      $arrExperimentInfo["mcm7"]["members"] = ["CTRL", "MCM7" ];
      $arrExperimentInfo["mcm7"]["hours"] = ["72"];
      $arrExperimentInfo["mcm7"]["phases"] = [0 => "", 3 => "_G0-G1", 4 => "_G2-M", 5 => "_S"];

      foreach ($arrMCM7experiments as $k => $numExperiment) {

      		if($exp == "mcm7") {

      			$txtFolderName = "histogram" . $numExperiment;
	    		$this->txtExperimentName = $txtFolderName;
	    		$arrExperimentInfo[$numExperiment] = $arrExperimentInfo["mcm7"];

      		}
 			else $numExperiment = $exp;
	    

    	foreach ($arrExperimentInfo[$numExperiment]["hours"] as $numHoursNr => $numHours) {

	         // Input file that will be cloned
	         $inputFileName = './sample/Histogram_Analysis_sample_merged.xlsx';

	         $objReader = PHPExcel_IOFactory::createReader('Excel2007');
	         $this->objPHPExcelMerged = $objReader->load($inputFileName);

	         // Set document properties
	         self::LogMe(date('H:i:s') . " Set document properties for MERGED file");
	         $this->objPHPExcelMerged->getProperties()->setCreator("Zoltan Dul")
	                               ->setLastModifiedBy("Zoltan Dul")
	                               ->setTitle("Histogram File for " . $numExperiment. " Experiment")
	                               ->setSubject("Flow Cytometry Analysis - Histogram")
	                               ->setDescription("Flow Cytometry analysis data, created histogram output file.")
	                               ->setKeywords("histogram " . implode(" ", $arrExperimentInfo[$numExperiment]["members"]))
	                               ->setCategory("Result file");

	         $txtOutputExcelMergedName = "output/Experiment" . $numExperiment . "_Histogram_Analysis_MERGED-".$numHours."h.xlsx";

	         foreach ($arrExperimentInfo[$numExperiment]["members"] as $numElement => $txtMutant) {

	            if($numElement == 0) continue;
	            else $this->numElement = $numElement;

	            $txtFilenameRoot = "_" . $numHours . "_singlets";
	            $txtOutputExcelName = "output/Experiment".$numExperiment."_Histogram_Analysis_" . $arrExperimentInfo[$numExperiment]["members"][0] . "v" . $txtMutant . "-".$numHours."h.xlsx";

	            // Create new PHPExcel object
	            self::LogMe(date('H:i:s') . ' // ');
	            self::LogMe(date('H:i:s') . " CREATE new PHPExcel object from import <B>Experiment".$numExperiment."</B> - Mutant: <B>$txtMutant</B> - <B>$numHours</B> hours" );

	            // Sample File Name that is going to be cloned
	            $inputFileName = './sample/Histogram_Analysis_sample.xlsx';

	            /** Load $inputFileName to a PHPExcel Object **/
	            // $this->objPHPExcel = PHPExcel_IOFactory::load($inputFileName);

	            $objPHPExcel = $objPHPExcel_new = new PHPExcel();
	            $objReader = PHPExcel_IOFactory::createReader('Excel2007');
	            //$objReader->setIncludeCharts(TRUE);
	            $this->objPHPExcel = $objReader->load($inputFileName);

	            // Set document properties
	            self::LogMe(date('H:i:s') . " Set document properties for subfile ($txtMutant $numHours)");
	            $this->objPHPExcel->getProperties()->setCreator("Zoltan Dul")
	                                  ->setLastModifiedBy("Zoltan Dul")
	                                  ->setTitle("Histogram File for " . $numExperiment. " Experiment")
	                                  ->setSubject("Flow Cytometry Analysis - Histogram")
	                                  ->setDescription("Flow Cytometry analysis data, created histogram output file.")
	                                  ->setKeywords("histogram " . $txtMutant)
	                                  ->setCategory("Result file");

	            foreach ($arrExperimentInfo[$numExperiment]["phases"] as $numSpreadSheet => $txtPhaseName) {

	               $txtFileNameEnd = $txtFilenameRoot . $txtPhaseName . $txtFiletype;
	               $txtOutputFileName = "Experiment".$numExperiment."-MERGED-".$arrExperimentInfo[$numExperiment]["members"][0] . "-" . $txtMutant . $txtFilenameRoot . $txtPhaseName . $txtFiletype;
	                  
	               $txtCTRLfilename = $arrExperimentInfo[$numExperiment]["members"][0] . $txtFileNameEnd;
	               $txtMUTfilename = $txtMutant . $txtFileNameEnd;

	               $arrCTRLread = self::file_processor($txtFolderName, $txtCTRLfilename);
	               $arrMUTread = self::file_processor($txtFolderName, $txtMUTfilename);

	               self::ExcelFiller($numSpreadSheet, $numHours, $arrCTRLread, $arrMUTread, $txtMutant);

	               self::Kiir($txtOutputFileName, $arrCTRLread, $arrMUTread);

	            }

	            //$this->objPHPExcel->setActiveSheetIndex(1);
	            //$this->objPHPExcel->getActiveSheet()->setTitle('Hist '.$numHours.'h');

	            // Save Excel 2007 file
	            self::LogMe(date('H:i:s') . " Write to Excel2007 format for subfile ($txtMutant $numHours)" );
	            $callStartTime = microtime(true);

	            $objWriter = PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel2007');

	            //
	            //  If we set Pre Calculated Formulas to true then PHPExcel will calculate all formulae in the
	            //    workbook before saving. This adds time and memory overhead, and can cause some problems with formulae
	            //    using functions or features (such as array formulae) that aren't yet supported by the calculation engine
	            //  If the value is false (the default) for the Excel2007 Writer, then MS Excel (or the application used to
	            //    open the file) will need to recalculate values itself to guarantee that the correct results are available.
	            //

	            //$objWriter->setPreCalculateFormulas(true);
	            //$objWriter->setIncludeCharts(TRUE);

	            $objWriter->save($txtOutputExcelName);
	            $callEndTime = microtime(true);
	            $callTime = $callEndTime - $callStartTime;

	            self::LogMe(date('H:i:s') . " PHPExcel Object File written to " . str_replace('.php', '.xlsx', pathinfo($txtOutputExcelName, PATHINFO_BASENAME)));
	            self::LogMe(date('H:i:s') . ' Call time to write Workbook was <B>' . sprintf('%.4f',$callTime) . "</B> seconds");
	            self::LogMe(date('H:i:s') . ' Current memory usage: ' . (memory_get_usage(true) / 1024 / 1024) , " MB");
	            self::LogMe(date('H:i:s') . ' // ');

	            $this->objPHPExcel = null;
	         }

	         self::LogMe( date('H:i:s') . " Write to Excel2007 format for Merged $numHours file");
	         $callStartTime = microtime(true);
	         $objWriter = PHPExcel_IOFactory::createWriter($this->objPHPExcelMerged, 'Excel2007');

	         $objWriter->save($txtOutputExcelMergedName);
	         $callEndTime = microtime(true);
	         $callTime = $callEndTime - $callStartTime;

	         self::LogMe(date('H:i:s') . " <B>MERGED</B> File written to " . str_replace('.php', '.xlsx', pathinfo($txtOutputExcelMergedName, PATHINFO_BASENAME)) );
	         self::LogMe(date('H:i:s') . 'Call time to write Workbook was <B>' . sprintf('%.4f',$callTime) . "</B> seconds");

	         $this->objPHPExcelMerged = null;

	    } 

	    if($exp != "mcm7") break;

	  }  
      
      print self::TimeEnd($this->timestart);

      // Echo memory peak usage
      self::LogMe( date('H:i:s') . " Peak memory usage: " . (memory_get_peak_usage(true) / 1024 / 1024) , " MB");

      // Echo done
      self::LogMe(date('H:i:s') . " Done writing files");
      self::LogMe(date('H:i:s') . " Files have been created in " . getcwd());
      self::LogMe(date('H:i:s') . " END " . microtime(true));

      return true;
   }

   private function file_processor($folder, $fajl) {

      $fajl = $folder . "/" . $fajl;
      $hiba = "";

      $fajl_beolvas = fopen($fajl,"r");
      if(!$fajl_beolvas) $hiba .= "File read can't be processed for <b>" . $fajl . "</b> file!";

      if($hiba) die($hiba);
         
      $sor = 0;
      $lister = array();
         
      while (($sor_tartalom = fgets($fajl_beolvas)) !== false) {

         $mezo = explode(",",$sor_tartalom);

         if($sor == 0) {

            if(strpos(trim($mezo[15]), "PE-A") ) $numero = 15;
            elseif(strpos(trim($mezo[14]), "PE-A") ) $numero = 14;
            elseif(strpos(trim($mezo[13]), "PE-A") ) $numero = 13;

         }

         $lister[$sor] = array();
         $lister[$sor]["fsc"] = trim($mezo[0]);
         $lister[$sor]["ssc"] = trim($mezo[3]); 
         $lister[$sor]["fitc"] = trim($mezo[6]); 
         $lister[$sor]["pe"] = trim($mezo[$numero]);

         $sor++;


      }

      return $lister;
   }

   private function logMe($txtLog) {

      $hiba = "";
      $txtFile = "output/" . $this->txtExperimentName . "-log.csv";

      if(!$this->itrLogFile) {
         $this->itrLogFile = fopen($txtFile,"w+");
         if(!$this->itrLogFile) $hiba .= "File read can't be processed for <b>" . $txtFile . "</b> file!";
      }

      if($hiba != "") die($hiba);

      fwrite($this->itrLogFile, strip_tags($txtLog) . "\n");
      echo $txtLog . EOL;

      return true;
   }

   private function ExcelFiller($numSpreadSheet, $numHours, $arrCTRL, $arrMUT, $txtMutant){

      $arrNumToRow = [];
      $arrNumToRow[1] = ["B","G","L","Q"];
      $arrNumToRow[2] = ["C","H","M","R"];
      $arrNumToRow[3] = ["D","I","N","S"];
      $arrNumToRow[4] = ["E","J","O","T"];

      $this->objPHPExcel->setActiveSheetIndex($numSpreadSheet);
      $this->objPHPExcelMerged->setActiveSheetIndex($numSpreadSheet);

      foreach ($arrCTRL as $numRow => $arrRowData) {

         $numThisRow = $numRow + 1;

         $this->objPHPExcel->getActiveSheet()->setCellValue('A' . $numThisRow, $arrRowData["fsc"])
                              ->setCellValue('C' . $numThisRow, $arrRowData["ssc"])
                              ->setCellValue('E' . $numThisRow, $arrRowData["fitc"])
                              ->setCellValue('G' . $numThisRow, $arrRowData["pe"]);
         
      }

      if($this->numElement == 1) {

         foreach ($arrCTRL as $numRow => $arrRowData) {

            $numThisRow = $numRow + 1;

            $this->objPHPExcelMerged->getActiveSheet()->setCellValue('A' . $numThisRow, $arrRowData["fsc"])
                                 ->setCellValue('F' . $numThisRow, $arrRowData["ssc"])
                                 ->setCellValue('K' . $numThisRow, $arrRowData["fitc"])
                                 ->setCellValue('P' . $numThisRow, $arrRowData["pe"]);
         
         }

      }

      foreach ($arrMUT as $numRow => $arrRowData) {

         $numThisRow = $numRow + 1;

         $this->objPHPExcel->getActiveSheet()->setCellValue('B' . $numThisRow, $arrRowData["fsc"])
                              ->setCellValue('D' . $numThisRow, $arrRowData["ssc"])
                              ->setCellValue('F' . $numThisRow, $arrRowData["fitc"])
                              ->setCellValue('H' . $numThisRow, $arrRowData["pe"]);

         $this->objPHPExcelMerged->getActiveSheet()->setCellValue($arrNumToRow[$this->numElement][0] . $numThisRow, $arrRowData["fsc"])
                              ->setCellValue($arrNumToRow[$this->numElement][1] . $numThisRow, $arrRowData["ssc"])
                              ->setCellValue($arrNumToRow[$this->numElement][2] . $numThisRow, $arrRowData["fitc"])
                              ->setCellValue($arrNumToRow[$this->numElement][3] . $numThisRow, $arrRowData["pe"]);
         
      }

      $this->objPHPExcelMerged->getActiveSheet()->setCellValue($arrNumToRow[$this->numElement][0] . "1", $txtMutant)
                              ->setCellValue($arrNumToRow[$this->numElement][1] . "1", $txtMutant)
                              ->setCellValue($arrNumToRow[$this->numElement][2] . "1", $txtMutant)
                              ->setCellValue($arrNumToRow[$this->numElement][3] . "1", $txtMutant);


      return true;
   }

   private function Kiir($fajl, $ctrl, $mutant){

      $hiba = "";
      $outputFolder = "output/";

      $fajl_beolvas = fopen($outputFolder . $fajl,"w+");
      if(!$fajl_beolvas) $hiba .= "File read can't be processed for <b>" . $outputFolder . $fajl . "</b> file!";

      if($hiba) die($hiba);

      $countCTRL = count($ctrl);
      $countMUT = count($mutant);

      $count_max = ( ($countCTRL <= $countMUT) ? $countMUT : $countCTRL );

      $liner = "";

      for ($i=0; $i < $count_max; $i++) {

         if($countCTRL > $i && $countMUT > $i)
            $liner .= $ctrl[$i]["fsc"] . "," . $mutant[$i]["fsc"] . "," . $ctrl[$i]["ssc"] . "," . $mutant[$i]["ssc"] . "," . $ctrl[$i]["fitc"] . "," . $mutant[$i]["fitc"] . "," . $ctrl[$i]["pe"] . "," . $mutant[$i]["pe"] . "\n";
         elseif ($countCTRL > $i ) $liner .= $ctrl[$i]["fsc"] . ",," . $ctrl[$i]["ssc"] . ",," . $ctrl[$i]["fitc"] . ",," . $ctrl[$i]["pe"] . ",," . "\n";
         else $liner .= "," . $mutant[$i]["fsc"] . ",," . $mutant[$i]["ssc"] . ",," . $mutant[$i]["fitc"] . ",," . $mutant[$i]["pe"] . "\n";

         if($i % 500) {
            fwrite($fajl_beolvas, $liner);
            $liner = "";
         }

      }

      fwrite($fajl_beolvas, $liner);

      return true;
   }

   private function TimeEnd($time_start, $plustxt = "Overall") {

         $time_end = microtime(true);
         $exection_time = $time_end - $time_start;

         $hours = (int) ($exection_time / 3600);
         $minutes = ( (int) ($exection_time / 60) ) - ($hours * 60);
         $seconds = $exection_time - ( ( $hours * 3600 ) + ( $minutes * 60 ) );

         $txt = $hours . " hours " . $minutes . " minutes and " . substr($seconds, 0, 5) . " seconds. [" . $exection_time . "]";

         return "<p>The <i>$plustxt</i> execution time was $txt</p>\n";
   }

}

// Script Run

   if( isset($_GET["exp"]) ) {

      $eredmeny = new Lekeres($_GET["exp"]);
   }
   else print "There were no <B>\$_GET[\"exp\"]</B> parameter defined!";

