<?php

/* (c) Dúl Zoltán 2016 */

error_reporting(E_ALL);
ini_set('display_errors', 'on');
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '-1');
ini_set('auto_detect_line_endings', true);

/*

USAGE:

$_GET @param mut = protein name


*/

//osztályok

class Lekeres {

	public $hiba = "";
	private $timestart;

	public function __construct($mit) {

		$this->timestart = microtime(true);

		$root = "_singles";
		$filetype = ".csv";
		$mut = $mit;

		$ending = ["", "_S", "_G2-M", "_G1", "_G0"];
		$mikor = [40, 48, 64, 72, 120];

		foreach ($mikor as $num => $hours) {

			$filename_root = "_" . $hours . $root;

			foreach ($ending as $num => $end) {

				$filename = $filename_root . $end . $filetype;
				$kiir_filename = "MERGED-CTRL-" . $mut . $filename_root . $end . $filetype;
					
				$this_CTRLfilename = "CTRL" . $filename;
				$this_MUTfilename = $mut . $filename;

				$ctrl = self::file_processor($this_CTRLfilename);
				$mutant = self::file_processor($this_MUTfilename);

				self::Kiir($kiir_filename, $ctrl, $mutant);

			}

		}
		
		print self::TimeEnd($this->timestart);

		return true;
	}


	private function file_processor($fajl) {

		$fajl = "histogram/" . $fajl;
		$hiba = "";

		$fajl_beolvas = fopen($fajl,"r");
		if(!$fajl_beolvas) $hiba .= "File read can't be processed for <b>" . $fajl . "</b> file!";

		if($hiba) die($hiba);
			
		$sor = 0;
		$lister = array();
			
		while (($sor_tartalom = fgets($fajl_beolvas)) !== false) {

			$mezo = explode(",",$sor_tartalom);

			if($sor == 0) {

				if(strpos(trim($mezo[15]), "PE-A") ) $numero = 15;
				elseif(strpos(trim($mezo[14]), "PE-A") ) $numero = 14;
				elseif(strpos(trim($mezo[13]), "PE-A") ) $numero = 13;

			}

			$lister[$sor] = array();
			$lister[$sor]["fsc"] = trim($mezo[0]);
			$lister[$sor]["ssc"] = trim($mezo[3]); 
			$lister[$sor]["fitc"] = trim($mezo[6]); 
			$lister[$sor]["pe"] = trim($mezo[$numero]);

			$sor++;


		}

		return $lister;
	}

	private function Kiir($fajl, $ctrl, $mutant){

		$hiba = "";
		$outputFolder = "output/";

		$fajl_beolvas = fopen($outputFolder . $fajl,"w+");
		if(!$fajl_beolvas) $hiba .= "File read can't be processed for <b>" . $outputFolder . $fajl . "</b> file!";

		if($hiba) die($hiba);

		$countCTRL = count($ctrl);
		$countMUT = count($mutant);

		$count_max = ( ($countCTRL <= $countMUT) ? $countMUT : $countCTRL );

		$liner = "";

		for ($i=0; $i < $count_max; $i++) {

			if($countCTRL > $i && $countMUT > $i)
				$liner .= $ctrl[$i]["fsc"] . "," . $mutant[$i]["fsc"] . "," . $ctrl[$i]["ssc"] . "," . $mutant[$i]["ssc"] . "," . $ctrl[$i]["fitc"] . "," . $mutant[$i]["fitc"] . "," . $ctrl[$i]["pe"] . "," . $mutant[$i]["pe"] . "\n";
			elseif ($countCTRL > $i ) $liner .= $ctrl[$i]["fsc"] . ",," . $ctrl[$i]["ssc"] . ",," . $ctrl[$i]["fitc"] . ",," . $ctrl[$i]["pe"] . ",," . "\n";
			else $liner .= "," . $mutant[$i]["fsc"] . ",," . $mutant[$i]["ssc"] . ",," . $mutant[$i]["fitc"] . ",," . $mutant[$i]["pe"] . "\n";

			if($i % 500) {
				fwrite($fajl_beolvas, $liner);
				$liner = "";
			}

		}

		fwrite($fajl_beolvas, $liner);

		return true;
	}

	private function TimeEnd($time_start, $plustxt = "Overall") {

			$time_end = microtime(true);
			$exection_time = $time_end - $time_start;

			$hours = (int) ($exection_time / 3600);
			$minutes = ( (int) ($exection_time / 60) ) - ($hours * 60);
			$seconds = $exection_time - ( ( $hours * 3600 ) + ( $minutes * 60 ) );

			$txt = $hours . " hours " . $minutes . " minutes and " . substr($seconds, 0, 5) . " seconds. [" . $exection_time . "]";

			return "<p>The <i>$plustxt</i> execution time was $txt</p>\n";
		}

}

// Script Run

	if( isset($_GET["mutant"]) ) $eredmeny = new Lekeres($_GET["mutant"]);
	else print "There were no <B>\$_GET[\"mutant\"]</B> parameter defined!";

?>
