<?php

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
ini_set('max_execution_time', '-1');
ini_set('memory_limit', '512M');
date_default_timezone_set('Europe/London');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/../PHPExcel/Classes/PHPExcel.php';

$txtOutputExcelName = "Histogram.xlsx";

// Create new PHPExcel object
echo date('H:i:s') , " Create new PHPExcel object from import" , EOL;

// Sample File Name that is going to be cloned
$inputFileName = './sample/Histogram_Analysis_sample3.xlsx';

$objPHPExcel = new PHPExcel();
$objReader = PHPExcel_IOFactory::createReader('Excel2007');
$objReader->setIncludeCharts(TRUE);
$objPHPExcel = $objReader->load($inputFileName);

// Save Excel 2007 file
echo date('H:i:s') , " Write to Excel2007 format" , EOL;

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

$objWriter->setIncludeCharts(TRUE);
$objWriter->save($txtOutputExcelName);

?>
