<HTML><?php

/* (c) Zoltán Dul 2015 */
/* Complexes Hanlder */
/* File (1) initialiser */


error_reporting(E_ALL);
ini_set('display_errors', 'on');
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '-1');

/* Kézzel az összes fájl behívása */

	$f1 = "source/";
	$f3 = "output/";

	$fajl1list = array();
	$fajl1list["ad"] = $f1 . "HS_ortholog_dbs_merged_AD2.csv";
	$fajl1list["pd"] = $f1 . "HS_ortholog_dbs_merged_PD2.csv";	
	$fajl1list["als"] = $f1 . "HS_ortholog_dbs_merged_ALS2.csv";
	$fajl2ann = $f1 . "vale_annot_new.csv";
	$fajl3 = $f3 . "vale2_OUTPUT.csv";

function FileRead($fajl, $how = false) {

	$szetszed1 = "\n";
	$szetszed2 = "\r";
	$hiba = "";

	$fajl_beolvas = fopen($fajl, "r");
	if(!$fajl_beolvas) $hiba .= "Nem tudtam beolvasni a $how - <b>" . $fajl . "</b> fájlt hozzáadásra!";

	$fajl_tartalom = fread($fajl_beolvas, filesize($fajl));
	$ujsor = explode($szetszed1,$fajl_tartalom);
	if(count($ujsor) < 3 ) $ujsor = explode($szetszed2,$fajl_tartalom);

	if($hiba != "") die($hiba);

	/* RESULT  */

	$sor = 0;
	$ReturnValues = array();

	foreach ($ujsor as $sor_id => $sor_tartalom) {

		$sor++;

		if ( empty($sor_tartalom) ) continue;
		$mezo = explode(";",$sor_tartalom);

		if( $how == "ann" ) $ReturnValues[trim($mezo[0])] = trim($sor_tartalom);

		else {

			$prot = trim($mezo[1]);
			$orth = trim($mezo[3]);

			$info = "";
			$nr = 0;

			for ($i=4; $i < 10; $i++) { 
				if(empty($mezo[$i])) continue;
				$nr++;
				$info .= trim($mezo[$i]) . ", ";
			}

			$info = trim($mezo[2]) . ";" . trim($mezo[3]) . ";" . $nr . " (" . substr($info, 0, -4) . "), ";

			if(array_key_exists($prot, $ReturnValues)) $ReturnValues[$prot] .= $info;
			else $ReturnValues[$prot] = $info;

		}

	}

	return $ReturnValues;
}

function FajlOrder($list, $ord) {

	$ReturnValues = array();

	foreach ($ord as $prot => $tobb) {

		if(!array_key_exists($prot, $list)) continue;

		$thisRow = $tobb . ";" . substr($list[$prot], 0, -2);

		$ReturnValues[] = $thisRow;

	}

	return $ReturnValues;

}

function FileWrite($fajl, $PrintThisOut) {

	$hiba = "";

	$fajl_beolvas = fopen($fajl,"w");
	if(!$fajl_beolvas) $hiba .= "Nem tudtam beolvasni írásra a <b>" . $fajl. "</b> fájlt hozzáadásra!";

	if($hiba != "") die($hiba);

	fwrite($fajl_beolvas, $PrintThisOut);
	fclose($fajl_beolvas);
}

function Valentina($fajl_list, $fajl_ann, $fajl_output) {

	$PrintThisOut = "";

	$list = FileRead($fajl_list["pd"], "list");
	$ann = FileRead($fajl_ann, "ann");

	$order = FajlOrder($list, $ann);

	foreach ($order as $key => $value) {
		$PrintThisOut .= $value ."\n";
	}

	FileWrite($fajl_output, $PrintThisOut);

}

Valentina($fajl1list, $fajl2ann, $fajl3 );

?></html>