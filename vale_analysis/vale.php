<HTML><?php

/* (c) Zoltán Dul 2015 */
/* Complexes Hanlder */
/* File (1) initialiser */


error_reporting(E_ALL);
ini_set('display_errors', 'on');
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '-1');

/* Kézzel az összes fájl behívása */

	$f1 = "source/";
	$f3 = "output/";

	$fajl1list = $f1 . "vale_list.csv";
	$fajl2ann = $f1 . "vale_annotation.csv";
	$fajl2ord = $f1 . "vale_order.csv";
	$fajl3 = $f3 . "vale_OUTPUT.csv";

function FileRead($fajl, $how = false) {

	$szetszed1 = "\n";
	$szetszed2 = "\r";
	$hiba = "";

	$fajl_beolvas = fopen($fajl, "r");
	if(!$fajl_beolvas) $hiba .= "Nem tudtam beolvasni a $how - <b>" . $fajl . "</b> fájlt hozzáadásra!";

	$fajl_tartalom = fread($fajl_beolvas, filesize($fajl));
	$ujsor = explode($szetszed1,$fajl_tartalom);
	if(count($ujsor) < 3 ) $ujsor = explode($szetszed2,$fajl_tartalom);

	if($hiba != "") die($hiba);

	/* RESULT  */

	$sor = 0;
	$ReturnValues = array();

	foreach ($ujsor as $sor_id => $sor_tartalom) {

		$sor++;

		if ( empty($sor_tartalom) ) continue;
		$mezo = explode(";",$sor_tartalom);

		if( $how == "ann" ) $ReturnValues[trim($mezo[1])] = trim($mezo[0]);

		elseif( $how == "ord") $ReturnValues[] = array(trim($mezo[0]), trim($mezo[1]));

		else {

			$prot = trim($mezo[0]);
			$orth = trim($mezo[1]);

			$info = "";
			$nr = 0;

			for ($i=2; $i < 8; $i++) { 
				if(empty($mezo[$i])) continue;
				$nr++;
				$info .= trim($mezo[$i]) . ", ";
			}

			$info = $nr . " (" . substr($info, 0, -2) . ")";

			if(! array_key_exists($prot, $ReturnValues)) $ReturnValues[$prot] = array($orth => $info);
			else $ReturnValues[$prot][$orth] = $info;

		}

	}

	return $ReturnValues;
}

function FajlMerge($list, $ann) {

	$ReturnValues = array();

	foreach ($list as $prot => $arri) {

		$a = "";

		foreach ($arri as $orth => $info) {
			$a .= $orth . ": " . $info . ", ";
		}

		$ReturnValues[$ann[$prot]] = substr($a, 0, -2);
		
	}

	return $ReturnValues;

}

function FajlOrder($list, $ord) {

	$ReturnValues = array();

	foreach ($ord as $n => $arr) {

		$thisRow = "";

		foreach ($arr as $n1 => $prot) {
			if(array_key_exists($prot, $list)) $thisRow .= $prot . ";" . $list[$prot] . ";";
			else $thisRow .= $prot . ";n/a;";
		}

		$ReturnValues[] = $thisRow;

	}

	return $ReturnValues;

}

function FileWrite($fajl, $PrintThisOut) {

	$hiba = "";

	$fajl_beolvas = fopen($fajl,"w");
	if(!$fajl_beolvas) $hiba .= "Nem tudtam beolvasni írásra a <b>" . $fajl. "</b> fájlt hozzáadásra!";

	if($hiba != "") die($hiba);

	fwrite($fajl_beolvas, $PrintThisOut);
	fclose($fajl_beolvas);
}

function Valentina($fajl_list, $fajl_ann, $fajl_ord, $fajl_output) {

	$PrintThisOut = "";

	$list = FileRead($fajl_list, "list");
	$ann = FileRead($fajl_ann, "ann");
	$ord = FileRead($fajl_ord, "ord");

	$ann_list = FajlMerge($list, $ann);
	$order = FajlOrder($ann_list, $ord);

	foreach ($order as $key => $value) {
		$PrintThisOut .= $value ."\n";
	}

	FileWrite($fajl_output, $PrintThisOut);

}

Valentina($fajl1list, $fajl2ann, $fajl2ord, $fajl3 );

?></html>