<HTML><?php

/* (c) Zoltán Dul 2014 */
/* FlyBase Handler */
/* File (3) mapper */

error_reporting(E_ALL);
ini_set('display_errors', 'on');
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '-1');


/* Kézzel az összes fájl behívása */
	
	$hiba = "";
	$szetszed1 = "\n";
	$szetszed2 = "\r";

	$f1 = "source/";
	$f2 = "mappings/";
	$f3 = "output/";

	$fajl1 = $f1 . "list_neurocrassa_orthomcl.csv";
	$fajl2 = $f1 . "neurocrassa_mapping.csv";
	$fajl3 = $f3 . "neurocrassa_mapped.csv";
	
	$fajl_beolvas1 = fopen($fajl1,"r");
	if(!$fajl_beolvas1) $hiba .= "Nem tudtam beolvasni a 1. <b>" . $fajl1 . "</b> fájlt hozzáadásra!";

	$fajl_beolvas2 = fopen($fajl2,"r");
	if(!$fajl_beolvas2) $hiba .= "Nem tudtam beolvasni a 1. <b>" . $fajl2 . "</b> fájlt hozzáadásra!";

	$fajl_beolvas3 = fopen($fajl3,"w");
	if(!$fajl_beolvas3) $hiba .= "Nem tudtam beolvasni a 1. <b>" . $fajl3 . "</b> fájlt hozzáadásra!";
	
	if($hiba != "") die($hiba);
	
/* beolvasása */

$lek_list = array();

$fajl_tartalom = fread($fajl_beolvas1, filesize($fajl1));
$ujsor = explode($szetszed1,$fajl_tartalom);
if(count($ujsor) < 3 ) $ujsor = explode($szetszed2,$fajl_tartalom);

foreach ($ujsor as $sor_id => $sor_tartalom) {

	if ( empty($sor_tartalom) ) continue;
	
	$mezo = explode(";",$sor_tartalom);	
	if(!array_key_exists(substr(trim($mezo[0]), 0, 8), $lek_list)) $lek_list[substr(trim($mezo[0]), 0, 8)] = array(trim($mezo[0]));
	elseif(!in_array(trim($mezo[0]), $lek_list[substr(trim($mezo[0]), 0, 8)])) $lek_list[substr(trim($mezo[0]), 0, 8)][] = trim($mezo[0]);
}


$fajl_tartalom = fread($fajl_beolvas2, filesize($fajl2));
$ujsor = explode($szetszed1,$fajl_tartalom);
if(count($ujsor) < 3 ) $ujsor = explode($szetszed2,$fajl_tartalom);

/* RESULT  */

$sor = 0;
$printelni = "";
$printelni_nincs = "";

foreach ($ujsor as $sor_id => $sor_tartalom) {

	if ( empty($sor_tartalom) ) continue;	
	$mezo = explode(";",$sor_tartalom);
	
	if(array_key_exists(trim($mezo[0]), $lek_list)) {

		foreach ($lek_list[trim($mezo[0])] as $key => $lekert_value) {
			
			$this_row = $lekert_value . ";" . trim($mezo[1]) . "\n";

			$printelni .= $this_row;
			$sor++;

			if( ($sor % 500) == 0){
				fwrite($fajl_beolvas3, $printelni);
				$printelni = "";
			}
		}
	}
}

fwrite($fajl_beolvas3, $printelni);

fclose($fajl_beolvas1);
fclose($fajl_beolvas2);
fclose($fajl_beolvas3);

print "A lekérés <B>".(count($lek_list, COUNT_RECURSIVE) - count($lek_list))."</B> iD-t tartalmazott. A lekérésből <B>$sor</B> keszult! :)";
?></HTML>