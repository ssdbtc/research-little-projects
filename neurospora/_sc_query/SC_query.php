<?php

/* (c) Dúl Zoltán 2012 */
/* ADATBÁZISOKBÓL LEKÉRŐ adott ID alapján */
/* Hibák mutatása és futési  php beállítása */

error_reporting(E_ALL);
ini_set('display_errors', 'on');
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '-1');

//osztályok



class Lekeres {

	public $hiba = "";
	public $szetszed1 = "\n";
	public $szetszed2 = "\r";
	public $sorsz = 0;
	public $lista;
	public $cc;
	private $kiir_fajl = "";
	private $kiiras = false;
	private $kiiras_beolv = "";
	private $printelni = "";
	private $dbt = array();
	private $fajok = array();
	private $mappings = array();
	private $container = array();

	public function Lekeres($files) {

		$this->kiir_fajl = $files["output"];
		$this->lista = self::leker_lista($files["list"], false);
		$dbs = self::leker_db($files["dbs"], "S.cerevisiae", $this->lista);

		return true;

	}

	private function leker_lista($fajl, $t = false, $keresett_faj = false) {

		$fajl_beolvas = fopen($fajl,"r");
		if(!$fajl_beolvas) $hiba .= "Nem tudtam beolvasni a 1. <b>" . $fajl . "</b> fájlt hozzáadásra!";

		$fajl_tartalom = fread($fajl_beolvas, filesize($fajl));
		$ujsor = explode($this->szetszed1,$fajl_tartalom);
		if(count($ujsor) < 3 ) $ujsor = explode($this->szetszed2,$fajl_tartalom);
			
		$sor = 0;
		$lister = array();
			
		foreach ($ujsor as $sor_id => $sor_tartalom) {
					
			if ( empty($sor_tartalom) ) continue;
			else {				
				$mezo = explode(";",$sor_tartalom);

				if(!$t){
					if(!in_array(trim($mezo[0]), $lister)) $lister[] = trim($mezo[0]);	
				}
				else {

					$faj1 = trim($mezo[0]);
					$unip1 = trim($mezo[1]);
					$faj2 = trim($mezo[2]);
					$unip2 = trim($mezo[3]);
					$perc = trim($mezo[4]);
					$group = trim($mezo[5]);
					$db = trim($mezo[6]);
					if(!in_array(substr($db, 0, 6), $this->dbt)) $this->dbt[] = substr($db, 0, 6);
					
					if($faj1 == $keresett_faj) {

						$sorsz = $db . "." . $unip1 . "." . $unip2;

						if(! array_key_exists($unip1, $this->mappings)) $this->mappings[$unip1] = array($unip2 => array($sorsz));
						elseif(! array_key_exists($unip2, $this->mappings[$unip1])) $this->mappings[$unip1][$unip2] = array($sorsz);
						elseif(! in_array($sorsz, $this->mappings[$unip1][$unip2])) $this->mappings[$unip1][$unip2][] = $sorsz;
						else continue;

						$this->container[$sorsz] = array("unip" => $unip1, "faj" => $faj2, "unip2" => $unip2, "perc" => $perc, "group" => $group, "db" => $db);
						$this->fajok[$unip2] = $faj2;
					}
					elseif($faj2 == $keresett_faj) {

						$sorsz = $db . "." . $unip2 . "." . $unip1;

						if(! array_key_exists($unip2, $this->mappings)) $this->mappings[$unip2] = array($unip1 => array($sorsz));
						elseif(! array_key_exists($unip1, $this->mappings[$unip2])) $this->mappings[$unip2][$unip1] = array($sorsz);
						elseif(! in_array($sorsz, $this->mappings[$unip2][$unip1])) $this->mappings[$unip2][$unip1][] = $sorsz;
						else continue;

						$this->container[$sorsz] = array("unip" => $unip2, "faj" => $faj1, "unip2" => $unip1, "perc" => $perc, "group" => $group, "db" => $db);
						$this->fajok[$unip1] = $faj1;

					}
					else continue;
				}				
				$sor++;
			}	
		}

		if($t) return $lister;
		return true;
	}

	private function leker_db($files, $keresett_faj, $lista) {

			$conts = array();
			foreach ($files as $key => $file) {
				$conts[] = self::leker_lista($file, true, $keresett_faj);
			}
			
			$sor = 0;
			$cc = 0;

			$this_row_array_new = array();

			foreach ($this->dbt as $key => $value) {
				$this_row_array_new[$value] = "";
			}

			
			foreach ($this->mappings as $unip1 => $arri) {
					
				foreach ($arri as $unip2 => $arri2) {

					$this_row = $unip1 .";". $this->fajok[$unip2] .";". $unip2 .";";
					$this_row_array = $this_row_array_new;

					foreach ($arri2 as $n => $sorsz) {
				
						$key = substr($sorsz, 0, 6);

						if($this->container[$sorsz]["perc"] == "n/a") $perc = "";
						else $perc = $this->container[$sorsz]["perc"];

						$this_row_array[$key] .= $this->container[$sorsz]["db"] . " (" . ( ($perc == "") ? "" : $perc.", " )  . $this->container[$sorsz]["group"] . "), ";
						
					}

					foreach ($this_row_array as $k => $v) {
						$this_row .= substr($v, 0, -2) . ";";
					}

					$this->printelni .= substr($this_row, 0, -1) . "\n";
					$sor++;

					if( ($sor % 500) == 0) self::Kiir();
				}
			}	

			self::Kiir();
			$this->sorsz = $sor;
			$this->cc = $cc;	
		}

	private function Kiir(){

			if(!$this->kiiras) {
				$hiba = "";
				
				$this->kiiras_beolv = fopen($this->kiir_fajl,"w+");
				if(!$this->kiiras_beolv) $hiba .= "Nem tudtam beolvasni a 1. <b>" . $this->kiir_fajl . "</b> fájlt hozzáadásra!";	
				if($hiba != "") die($hiba);

				$this->kiiras = true;
			}

			fwrite($this->kiiras_beolv, $this->printelni);
			$this->printelni = "";
			return true;
		}
}

/* Kézzel az összes fájl behívása */
	
	$hiba = "";
	$szetszed1 = "\n";
	$szetszed2 = "\r";
	$f1 = "source/";
	$f2 = "output/";

	$files = array();
	$files["list"] = $f1."SC_unip_list.txt";
	$files["output"] = $f2."SC_unip_list_query.csv";
	$files["dbs"] = array();
	$files["dbs"]["homologene"] = $f1."homologene_merged.csv";
	$files["dbs"]["orthomcl"] = $f1."orthomcl_merged.csv";
	$files["dbs"]["inparanoid"] = $f1."inparanoid.csv";
	$files["dbs"]["kog"] = $f1."kog_merged.csv";
	$files["dbs"]["eggnog"] = $f1."eggNOG_merged.csv";

	$eredmeny = new Lekeres($files);

/* RESULT  */

$sor = 0;
$printelni = "";
print "<textarea>\n";
print "</textarea>\n<BR>";
print "# <B>".$eredmeny->sorsz."</B> keszult! :) ".$eredmeny->cc."";
?>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
</head>
<body>

</body>
</html>