<?php

/* (c) Dúl Zoltán 2013 */
/* BIOGRID ADATBÁZISOKBÓL LEKÉRŐ adott ID alapján */
/* Hibák mutatása és futési  php beállítása */

error_reporting(E_ALL);
ini_set('display_errors', 'on');
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '-1');

/* Kézzel öszeállított keresési lista behívása */
	
	$fajl1 = "cosmicSUBSTv2.csv";

class FajlBeolvas {

	public $szetszed1 = "\n";
	public $szetszed2 = "\r";
	public $fajl_tomb;

	public function __construct($fajl) {

		$hiba = "";
		
		$fajl_beolvas = fopen($fajl,"r");
		if(!$fajl_beolvas) $hiba .= "Nem tudtam beolvasni a <b>". $fajl ."</b> fájlt (lekérő) hozzáadásra!";

		if($hiba != "") die($hiba);

		$fajl_tartalom = fread($fajl_beolvas, filesize($fajl));
		$ujsor = explode($this->szetszed1,$fajl_tartalom);
		if(count($ujsor) < 3 ) $ujsor = explode($this->szetszed2,$fajl_tartalom);
		
		$sor = 0;
		$lekeres_lista = array();
		
		foreach ($ujsor as $sor_id => $sor_tartalom) {
		
			$sor++;				
			if ( empty($sor_tartalom) or $sor == 1) continue;

			else {			
				$mezo = explode(";",$sor_tartalom);
				
				if(!array_key_exists(trim($mezo[0]), $lekeres_lista)) $lekeres_lista[trim($mezo[0])] = array(trim($mezo[1]));
				else $lekeres_lista[trim($mezo[0])][] = trim($mezo[1]);
				
				
			}
		
		}		

		$this->fajl_tomb = $lekeres_lista;
	}

}	

$cosmic = new FajlBeolvas($fajl1);


print "<textarea cols=50 rows=50>\n";

	foreach ($cosmic->fajl_tomb as $group => $arr) {

		print $group . "\t";

		$members = "";
		
		foreach ($arr as $k => $v) $members .= $v . " - ";

		print substr($members, 0, -3) . "\t";

		print count($arr);

		print "\n";

	}

print "</textarea>\n";

?>